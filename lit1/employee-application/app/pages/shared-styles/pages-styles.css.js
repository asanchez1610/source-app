import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
main {
  margin-top: 45px;
  background-color: #E9E9E9;
  display: flex;
  height: calc(100vh - 40px);
}
main .container-main {
  background-color: white;
  padding: 15px 25px;
  width: calc(100vw - 105px);
  margin: 25px;
  border: 1px solid rgba(165, 165, 165, 0.75);
  box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  -webkit-box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  -moz-box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  overflow: auto;
}
`;
