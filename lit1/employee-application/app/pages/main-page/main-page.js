import { html } from 'lit-element';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-form-textarea/bbva-web-form-textarea.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
import { CellsMasterPage } from '../../../app/utils/ui/CellsMasterPage.js';
import styles from './main-page.css.js';
import '@bbva-web-components/bbva-core-icon/bbva-core-icon.js';
import sharedStyles from '../../../app/pages/shared-styles/pages-styles.css.js';

class MainPage extends CellsMasterPage {
  static get is() {
    return 'main-page';
  }

  static get properties() {
    return {
      messages: Array
    };
  }

  static get styles() {
    return [
      styles,
      sharedStyles
    ];
  }

  constructor() {
    super();
    this.messages = [];
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener('on-core-portal-publish-hello-children', async({detail}) => {
      this.messages.unshift({
        msg: detail.msg,
        time: this.mmtjs((new Date())).format('DD/MM/YYYY HH:mm:ss')
      });
      await this.requestUpdate();
    });
  }

  async removeMsg(index) {
    this.messages = this.messages.filter((item, _index) => index !== _index);
    await this.requestUpdate();
  }

  async getDatPortal(type) {
    let dataResponse;
    let textResp = 'Sin información para mostrar';

    switch (type) {
      case 'user':
        dataResponse = await this.getUserPortalSession();
        if (dataResponse) {
          try {
            textResp = JSON.stringify(dataResponse);
          } catch (e) {
            console.log(e);
          }
        }
        this.element('#resp-user').innerHTML = textResp;
        break;

      case 'tsec':
        textResp = await this.getTSECPortal();
        this.element('#resp-tsec').innerHTML = textResp;
        break;

      case 'params':
        dataResponse = await this.getParamsDataChildren(this.appProps.applicationId);
        if (dataResponse) {
          try {
            textResp = JSON.stringify(dataResponse);
          } catch (e) {
            console.log(e);
          }
        }
        this.element('#resp-params').innerHTML = textResp;
        break;

      default:
        break;
    }


  }

  copyText(type) {
    let elementText;
    switch (type) {
      case 'user':
        elementText = this.element('#resp-user');
        break;
      case 'tsec':
        elementText = this.element('#resp-tsec');
        break;
      case 'params':
        elementText = this.element('#resp-params');
        break;

      default:
        elementText = null;
        break;
    }

    if (elementText) {
      const textArea = document.createElement('textarea');
      if (type === 'user' || type === 'params') {
        const _textInit = elementText.textContent;
        const _json = JSON.parse(_textInit);
        textArea.value = JSON.stringify(
          _json,
          null,
          2
        );
      } else {
        textArea.value = elementText.textContent;
      }
      document.body.appendChild(textArea);
      textArea.select();
      document.execCommand('Copy');
      textArea.remove();
    }

  }

  addExternalTab() {
    const label = this.element('bbva-web-form-text[name="title"]').value;
    const iconName = this.element('bbva-web-form-text[name="icon"]').value;
    let path = this.element('bbva-web-form-text[name="iframe"]').value;
    let dataParms = {};
    const jsonStr = this.element('bbva-web-form-textarea[name="jsonParams"]').value;
    if (jsonStr) {
      try {
        dataParms = JSON.parse(jsonStr);
      } catch (e) {
        console.log(e);
      }
    }
    const id = this.randomID(4);
    path = `${path}?appId=${id}`;
    this.addTabPortal({ path, iconName, label, id }, { dataParms });
  }

  render() {
    return this.content(html`
       <main>
          <div class="container-main" >
              <div class="welcome">

                <h3>Bienvenido a la Aplicación</h3>

                <p>La aplicación portal padre ha dejado los siguientes mensajes</p>

                <div class="empty-messages" ?hidden="${this.messages.length > 0}">Sin mensajes para mostrar</div>

                <div class="list-messages" >
                  ${this.messages.map((item, index) => html`
                  
                  <div>
                    <span class="text">
                      <span>Mensaje</span><br>
                      ${item.msg}
                    </span> 
                    <span class="text" >
                    <span>Hora</span><br>  
                   ${item.time}
                    </span>
                    <bbva-core-icon @click="${() => this.removeMsg(index)}" icon="bbva:close" size="20" ></bbva-core-icon>
                  </div>

                  
                  `)}
                  
                </div>

              </div>
              <div class="portal-data">

                <div class="box-data" >
                  <h2>
                    <div>Obtener datos de Usuario desde el Portal</div>
                    <bbva-web-button-default @click="${() => this.getDatPortal('user')}" >Probar</bbva-web-button-default>
                  </h2>
                  <div class="response" >
                    <span id="resp-user">Sin información para mostrar</span>
                    <div>
                      <bbva-core-icon title="copiar" icon="bbva:copy" size="24" @click="${() => this.copyText('user')}" ></bbva-core-icon>
                      <bbva-core-icon title="limpiar" class="trash" icon="bbva:trash" size="24" @click="${() => this.element('#resp-user').innerHTML = 'Sin información para mostrar'}" ></bbva-core-icon>
                    </div>
                  </div>
                </div>

                <div class="box-data" >
                  <h2>
                    <div>Obtener TSEC desde el Portal</div>
                    <bbva-web-button-default @click="${() => this.getDatPortal('tsec')}">Probar</bbva-web-button-default>
                  </h2>
                  <div class="response box-orange" >
                    <span id="resp-tsec">Sin información para mostrar</span>
                    <div>
                      <bbva-core-icon title="copiar" icon="bbva:copy" @click="${() => this.copyText('tsec')}" size="24" ></bbva-core-icon>
                      <bbva-core-icon title="limpiar" class="trash" icon="bbva:trash" size="24" @click="${() => this.element('#resp-tsec').innerHTML = 'Sin información para mostrar'}" ></bbva-core-icon>
                    </div>
                  </div>
                </div>

                <div class="box-data" >
                  <h2>
                    <div>Obtener datos Parametros enviados desde el portal</div>
                    <bbva-web-button-default @click="${() => this.getDatPortal('params')}">Probar</bbva-web-button-default>
                  </h2>
                  <div class="response box-purple" >
                    <span id="resp-params">Sin información para mostrar</span>
                    <div>
                      <bbva-core-icon title="copiar" icon="bbva:copy" @click="${() => this.copyText('params')}" size="24" ></bbva-core-icon>
                      <bbva-core-icon title="limpiar" class="trash" icon="bbva:trash" size="24" @click="${() => this.element('#resp-params').innerHTML = 'Sin información para mostrar'}" ></bbva-core-icon>
                    </div>
                  </div>
                </div>

                <div class="box-data" >
                  <h2>
                    <div>Agregar nuevo Tab App Externa</div>
                    <bbva-web-button-default @click="${this.addExternalTab}" >Probar</bbva-web-button-default>
                  </h2>
                  <div class="content-add-tab" >
                    <div class="inputs" >
                      <bbva-web-form-text name="title" class="input-titulo" label="Título" ></bbva-web-form-text>
                      <bbva-web-form-text name="icon" class="input-icon" label="Icono" ></bbva-web-form-text>
                      <bbva-web-form-text name="iframe" class="input-iframe" label="Url Iframe" ></bbva-web-form-text>
                    </div>
                    <bbva-web-form-textarea
                        name="jsonParams"
                        class="json-input"
                        id="body-params-tab"
                        label="Params (JSON)"
                    ></bbva-web-form-textarea>
                  </div>
                </div>

                <div class="box-data" >
                  <h2>
                    <div>Enviar notificaciones</div>
                    <div>
                      <bbva-web-button-default style="margin-right: 10px;" @click="${() => this.showNotificationPortal('¡Genial! Has enviado una notificación desde una sub-aplicación.', 'success')}" >Toast</bbva-web-button-default> 
                      <bbva-web-button-default @click="${() => this.showModalSessionExpiredPortal()}" >Sesion expirada</bbva-web-button-default>
                    </div>
                  </h2>
              </div>

              </div>
          </div>
      </main>
    `, '{{applicationTitle}}');
  }
}
window.customElements.define(MainPage.is, MainPage);
