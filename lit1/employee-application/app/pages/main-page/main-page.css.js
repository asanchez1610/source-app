import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]), [hidden] {
  display: none !important;
}

.container-page {
  width: 100%;
  max-width: 100%;
  height: 100vh;
  background-color: #E9E9E9;
  overflow: hidden;
}

bbva-foundations-spinner {
  background-color: #004481;
  width: 100%;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
}

.hide {
  display: none;
}

.container-main {
  display: flex;
  margin-right: 10px;
}
.container-main .welcome {
  width: 40%;
}
.container-main .welcome h3 {
  font-weight: 100;
  font-size: 1.5em;
  text-align: center;
  margin: 0;
  padding: 0;
}
.container-main .welcome p {
  font-weight: lighter;
  font-size: 1em;
  margin: 0;
  padding: 0;
  font-style: italic;
  margin: 10px 0;
  color: #666666;
  text-align: center;
}
.container-main .welcome .empty-messages {
  padding: 10px;
  width: calc(100% - 20px);
  background-color: #f4f4f4;
  border: 1px solid #ccc;
  text-align: center;
}
.container-main .welcome .list-messages {
  display: flex;
  flex-direction: column;
  gap: 10px;
}
.container-main .welcome .list-messages div {
  position: relative;
  width: calc(100% - 30px);
  padding: 10px;
  border: 1px solid #B79E5E;
  background-color: #FEF5DC;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 0.9em;
}
.container-main .welcome .list-messages div .text span {
  font-size: 0.85em;
  color: #4D6572;
}
.container-main .welcome .list-messages div bbva-core-icon {
  position: absolute;
  top: 3px;
  right: 3px;
  color: #4D6572;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
}
.container-main .welcome .list-messages div bbva-core-icon:hover {
  color: #1D303D;
}
.container-main .portal-data {
  padding-left: 20px;
  padding-right: 20px;
  width: calc(60% - 20px);
  border-left: 1px solid rgba(0, 0, 0, 0.2);
  margin-left: 20px;
}
.container-main .portal-data .box-data {
  margin-bottom: 25px;
}
.container-main .portal-data .box-data h2 {
  font-weight: 100;
  font-size: 1.3em;
  border-bottom: 1px solid #e1e1e1;
  padding-bottom: 5px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
}
.container-main .portal-data .box-data h2 div {
  margin-top: 10px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
}
.container-main .portal-data .box-data h2 bbva-web-button-default {
  height: 38px;
  font-size: 0.7em;
  font-weight: 100;
}
.container-main .portal-data .box-data .response {
  background-color: #1D303D;
  color: white;
  padding: 15px;
  border-left: 10px solid #48AE64;
  font-size: 0.95em;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.container-main .portal-data .box-data .response span {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: calc(100% - 70px);
}
.container-main .portal-data .box-data .response bbva-core-icon {
  color: white;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
}
.container-main .portal-data .box-data .response bbva-core-icon:hover {
  color: #49A5E6;
}
.container-main .portal-data .box-data .response .trash:hover {
  color: #778892;
}
.container-main .portal-data .box-data .box-orange {
  border-left: 10px solid #F7893B;
}
.container-main .portal-data .box-data .box-purple {
  border-left: 10px solid #B6A8EE;
}
.container-main .portal-data .box-data .content-add-tab .inputs {
  display: flex;
  align-items: center;
  gap: 5px;
}
.container-main .portal-data .box-data .content-add-tab .inputs .input-titulo {
  width: 250px;
}
.container-main .portal-data .box-data .content-add-tab .inputs .input-icon {
  width: 150px;
}
.container-main .portal-data .box-data .content-add-tab .inputs .input-iframe {
  width: calc(100% - 400px);
}
.container-main .portal-data .box-data .content-add-tab .json-input {
  margin-top: 5px;
}

@media screen and (max-height: 815px) {
  .container-main .portal-data {
    padding-bottom: calc(100vh - 25px);
  }
  .container-main .welcome {
    height: calc(100vh - 120px);
    overflow: hidden;
    overflow-y: auto;
  }
}
`;
