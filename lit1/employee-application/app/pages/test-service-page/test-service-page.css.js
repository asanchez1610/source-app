import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
.container-page {
  width: 100%;
  max-width: 100%;
  height: 100vh;
  background-color: #E9E9E9;
  overflow: hidden;
}

bbva-foundations-spinner {
  background-color: #004481;
  width: 100%;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
}

.hide {
  display: none;
}

main {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  width: 100%;
  height: calc(100% - 110px);
}
main .container-main {
  display: flex;
}
main .container-main .form {
  width: 500px;
}
main .container-main .form * {
  margin-bottom: 5px;
}
main .container-main .form bbva-web-button-default {
  width: 100%;
}
main .container-main .form h2 {
  font-weight: lighter;
  color: #000;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 10px;
  padding-bottom: 5px;
  font-size: 1.35em;
}
main .container-main .response {
  width: calc(100% - 535px);
}
main .container-main .response textarea {
  width: calc(100% - 30px);
  height: calc(100vh - 180px);
  background-color: rgb(41, 45, 62);
  margin: 15px;
  resize: none;
  color: #ce93d8;
  outline: none;
  padding: 10px;
  font-size: 1.2em;
}
`;
