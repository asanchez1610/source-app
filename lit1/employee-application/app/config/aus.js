const appConfig = require('./sources/props.js');
const envConf =  { ...appConfig };

envConf.app_properties.services.host = 'https://aso-aus2-pe.work-02.nextgen.igrupobbva';
//envConf.app_properties.services.host = 'https://cal-glomo.bbva.pe/SRVS_A02';
envConf.app_properties.securityConf.externalDMZ = 'https://au-branches-i-pe.work-04.platform.bbva.com/index.html';
module.exports = envConf;
