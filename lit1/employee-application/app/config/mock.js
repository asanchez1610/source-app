const appConfig = require('./sources/props.js');
const envConf =  { ...appConfig };
/**
 * Configuration used in e2e:
 *
 *  cells app:e2e -u <url> -c <config_file>
 *
 * More info in README.md
 */
envConf.app_properties.services.host = 'http://localhost:3000';
envConf.app_properties.enableVBank = true;
envConf.app_properties.securityConf.externalDMZ = 'https://ei-community.grupobbva.com/';
module.exports = envConf;
