const appConfig = require('./sources/props.js');
const envConf =  { ...appConfig };

envConf.app_properties.services.host = 'https://aso-int2-pe.work-02.nextgen.igrupobbva';
//envConf.app_properties.services.host = 'https://tst-glomo.bbva.pe/SRVS_A02';
envConf.app_properties.securityConf.externalDMZ = 'https://branches-i-pe.live-04.platform.bbva.com/';
module.exports = envConf;
