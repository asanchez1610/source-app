const paths = () => {
  return {
    customers: {
      findByDocument:
        'customers/v0/customers?identityDocument.documentNumber=41523066&identityDocument.documentType.id=DNI'
    },
    session: {
      userDetail: 'business-agents/v0/business-agents?expand=applications,contact-details&id={userId}'
    }
  };
};

module.exports = {
  paths: paths,
};
