const optionsMenu = () => {
  return [
    {
      id: 1,
      name: 'MENU_1',
      label: 'Bienvenida',
      iconName: 'bbva:home',
      path: 'main',
      order: 1,
      componentType: {
        id: 1,
        name: 'MENU',
      },
    },
    {
      id: 2,
      name: 'MENU_2',
      label: 'Test de servicos',
      iconName: 'bbva:settings',
      path: 'test-service',
      order: 2,
      componentType: {
        id: 1,
        name: 'MENU',
      },
    }
  ];
};

module.exports = {
  optionsMenu: optionsMenu
};


