import { html, LitElement } from 'lit-element';
import { BbvaCoreIntlMixin as intl } from '@bbva-web-components/bbva-core-intl-mixin';
import { CellsCoreElementMixin as mxnCore } from '@cells-portals-cross-components/cells-portals-core-mixins-ppgc';
import { CellsPageMixin as cellsPage } from '@cells/cells-page-mixin';
import '../../../app/utils/ui/container-app-template.js';
import { BbvaCoreIconset } from '@bbva-web-components/bbva-core-iconset';


const _iconSet = new BbvaCoreIconset({
  name: 'bbva',
  width: 300,
  height: 300
});

export class CellsMasterPage extends intl(mxnCore(cellsPage(LitElement))) {

  static get properties() {
    return {
      appProps: Object,
      applicationParams: Object
    };
  }

  constructor() {
    super();
    this.appProps = window.AppConfig;
    this.channels = this.extract(this.appProps, 'crossChannels', {});
    this.host = this.extract(this.appProps, 'services.host', '');
    this.paths = this.extract(this.appProps, 'services.paths', {});
    this.securityConf = this.extract(this.appProps, 'securityConf', {});
    this.eventRequestName = this.extract(this.appProps,'ctts.eventRequestName', 'send-request-dp');
    this.constants = this.extract(this.appProps, 'ctts', {});
    this.applicationParams = {}
    this.getParamsDataChildren(this.appProps.applicationId).then(data => this.applicationParams = data);
    if (!window.isPresentListenerNavigate) {
      window.addEventListener('select-menu-item', ({detail}) => this.navigate(detail.path) );
      window.isPresentListenerNavigate = true;
    }
    
  }

  iconBBVA(iconName, options={}) {
    this.buildIcon(iconName, options.width, options.height, _iconSet);
  }

  getParentPortalData(eventName, options={}) {
    return new Promise((resolve, reject) => {
      let _count = 0;
      const { applicationId } = window.AppConfig;
      const _timer = setInterval(() => { _count = _count + 1; }, 1000);
      const _argBase = {
        eventName: eventName,
        applicationId: applicationId,
        withResponse: true,
        idAppIframe: options.idAppIframe || applicationId
      };
      const _arguments = { ..._argBase, ...options };
      const _evtResult = `${_arguments.eventName}-child-${applicationId}`;
      const _fnResult = (result) => {
        clearInterval(_timer);
        window.removeEventListener(_evtResult, _fnResult);
        switch (eventName) {
          case this.channels.userSession:
            const { userSession } = result.detail;
            resolve(userSession);
            break;
          case this.channels.tsec:
            const { tsec } = result.detail;
            resolve(tsec);
            break;
          case this.channels.listApps:
            const { applications } = result.detail;
            resolve(applications);
            break;
          case this.channels.childDataParams:
            const { paramsData } = result.detail;
            resolve(paramsData);
            break;
          case this.channels.sendNotification:
            resolve(true);
            break;
          case this.channels.showDialogExpiredSession:
            resolve(true);
            break;
          case this.channels.getComponents:
            const { components } = _result.detail;
            resolve(components);
            break;     
        }
      };
      window.addEventListener(_evtResult, _fnResult);
      this.glCommunicator._sendMessageToParent(_arguments);
      if (_count > 5) {
        clearInterval(_timer);
        reject(_arguments);
      }
    });
  }

  async getUserPortalSession(){
    try {
      return await this.getParentPortalData(this.channels.userSession);
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  async getTSECPortal(options={}){
    try {
      return await this.getParentPortalData(this.channels.tsec, options);
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  async getMenuApps(){
    try {
      return await this.getParentPortalData(this.channels.listApps);
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  async getParamsDataChildren(idAppIframe){
    try {
      return await this.getParentPortalData(this.channels.childDataParams, { idAppIframe });
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  async showNotificationPortal(message, type){
    try {
      return await this.getParentPortalData(this.channels.sendNotification, { message, type });
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  async showModalSessionExpiredPortal() {
    try {
      return await this.getParentPortalData(this.channels.showDialogExpiredSession, { });
    } catch(e) {
      console.log('Warning:', e);
      return {};
    }
  }

  addTabPortal(tab, dataParams) {
    const { applicationId } = window.AppConfig;
    const _arguments = {
      eventName: this.channels.addTab,
      applicationId: applicationId,
      tab,
      dataParams
    };
    this.glCommunicator._sendMessageToParent(_arguments);
  }

  get glAuthServ () {
    return document.querySelector('#auth-serv-gl-cmp');
  }

  get glSpinner () {
    return document.querySelector('#spinner-gl-cmp');
  }

  get glNotification () {
    return document.querySelector('#notification-gl-cmp');
  }

  get glMdlErrorAuth () {
    return document.querySelector('#modal-error-auth-serv-gl-cmp');
  }

  get glCommunicator () {
    return document.querySelector('#con-portal-dm-gl-cmp');
  }
  
  get glSubMenu() {
	  return document.querySelector('#sub-menu-gl-cmp');
  }

  get glDmSession() {
    return document.querySelector('#session-dm-gl-cmp');
  }

  showToastNoti({ message='', type='info' }) {
    this.glNotification.setAttribute('class', `noti-${type}`);
    this.glNotification.innerHTML = `<p>${message}</p>`;
    this.glNotification.opened = true;
  }

  async initMasterPage() {
    await this.updateComplete;
    this.overwriteOnPageMethods();
  }

  beforeOnPageEnter() {}

  beforeOnPageLeave() {}

  interceptorBeforeMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      fn.apply(object);
      originalMethod.apply(object, arguments);
    };
  }

  interceptorAfterMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      originalMethod.apply(object, arguments);
      fn.call(object);
    };
  }

  overwriteOnPageMethods() {
    if (this.onPageEnter && typeof this.onPageEnter === 'function') {
      this.interceptorBeforeMethod(this, 'onPageEnter', this.beforeOnPageEnter);
    }
    if (this.onPageLeave && typeof this.onPageLeave === 'function') {
      this.interceptorBeforeMethod(this, 'onPageLeave', this.beforeOnPageLeave);
    }
  }

  async awaitPresentTsec() {
    let clear;
    let count = 0;
    return new Promise((resolve, reject) => {
      clear = setInterval(() => {
        count = count + 1;
        const _tsec = window.sessionStorage.getItem(this.appProps.tsecNameSession);
        if (_tsec) {
          resolve(true);
          clearTimeout(clear);
        } else if (count > 80) {
          reject(false);
        }
      }, 500);
    });
  }

  content(contentHtml, titlePage='') {
    return html` <container-app-template page-title="${titlePage}">
      <div class="container-page" slot="app-main-content">
          ${contentHtml}
      </div>   
    </container-app-template>`;
  }
}
