const VALUES = {
  AC: 'auth-serv-gl-cmp',
  MDLAUTH: 'modal-error-auth-serv-gl-cmp',
  SPIN: 'spinner-gl-cmp',
  NOT: 'notification-gl-cmp',
  SES: 'session-dm-gl-cmp',
  CON: 'con-portal-dm-gl-cmp',
  SM: 'sub-menu-gl-cmp'
};

const hash = async (string) => {
  const utf8 = new TextEncoder().encode(string);
  return crypto.subtle.digest('SHA-512', utf8).then((hashBuffer) => {
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray
      .map((bytes) => bytes.toString(16).padStart(2, '0'))
      .join('');
    return hashHex;
  });
}

const callCommunicatorPortal = (eventName, options={}) =>{
  return new Promise((resolve, reject) => {
    let _count = 0;
    const { applicationId } = window.AppConfig;
    const _timer = setInterval(() => { _count = _count + 1; }, 1000);
    const _argBase = {
      eventName: eventName,
      applicationId: applicationId,
      withResponse: true,
      idAppIframe: options.idAppIframe || applicationId
    };
    const _arguments = { ..._argBase, ...options };
    const _evtResult = `${_arguments.eventName}-child-${applicationId}`;
    const _fnResult = (_result) => {
      clearInterval(_timer);
      window.removeEventListener(_evtResult, _fnResult);
      switch (eventName) {
         case window.AppConfig.crossChannels.showDialogExpiredSession:
          resolve(true);
          break;  
         case window.AppConfig.crossChannels.getComponents:
          const { components } = _result.detail;
          resolve(components);
          break;     
      }
    };
    window.addEventListener(_evtResult, _fnResult);
    document.querySelector(`#${VALUES.CON}`)._sendMessageToParent(_arguments);
    if (_count > 5) {
      clearInterval(_timer);
      reject(_arguments);
    }
  });
}

const awaitAppConf = () => {
  let clear;
  let count = 0;
  let accessControl = auhServ();
  return new Promise((resolve, reject) => {
    clear = setInterval(() => {
      count = count + 1;
      if (appConf() && accessControl && accessControl.begin) {
        resolve(true);
        clearTimeout(clear);
      } else if (count > 80) {
        reject(false);
      }
    }, 500);
  });
};

const showLoadingGlobalApp = (show, modeSpinner) => {
  const spinnerGlobal = document.querySelector(`#${VALUES.SPIN}`);
  if (show) {
    if (modeSpinner) {
      spinnerGlobal.classList.remove('hide');
      return;
    }
    auhServ().showMaskLoading();
  } else {
    if (modeSpinner) {
      spinnerGlobal.classList.add('hide');
      return;
    }
    auhServ().hideMaskLoading();
  }
};

const elDOM = (selector) => {
  return document.querySelector(selector);
};

const appConf = () => {
  return window.AppConfig;
};

const auhServ = () => {
  return elDOM(`#${VALUES.AC}`);
};

const awaitUserIdSession = () => {
  let clear;
  let count = 0;
  return new Promise((resolve, _reject) => {
    clear = setInterval(() => {
      count = count + 1;
      if (window.sessionStorage.getItem('userId')) {
        resolve(true);
        clearTimeout(clear);
      } else if (count > 80) {
        resolve(false);
      }
    }, 500);
  });
};

const applySessionConfg = async () => {
  auhServ().disableValidateSession = false;
  auhServ().urlGrantingTicket = `${appConf().services.host}/${
    appConf().securityConf.pathGT
  }`;
  auhServ().appId = appConf().securityConf.aapId;
  const enableVBank = appConf().enableVBank;
  const tsecNameSession = appConf().tsecNameSession || 'tsec';
  let tsec = window.sessionStorage.getItem(tsecNameSession);
  if (!tsec && !enableVBank) {
    await auhServ().begin();
  } else {
    auhServ().hideMaskLoading();
  }
  if (enableVBank) {
    tsec = await hash(tsecNameSession);
    window.sessionStorage.setItem(tsecNameSession, tsec);
    window.sessionStorage.setItem('userId', 'PXXXXXXX1');
  }
  await awaitUserIdSession();
};

const addListenersApp = () => {
  auhServ().addEventListener('employee-authentication-success', (_result) => {
    showLoadingGlobalApp(false);
    window.dispatchEvent(
      new CustomEvent('done-success-auth', {
        detail: true,
        bubbles: true,
        composed: true,
      })
    );
  });

  
  auhServ().addEventListener('idp-assertion-error', () => {
    console.log('idp-assertion-error');
    showLoadingGlobalApp(false);
    
    // == En el caso de que se desee mostrar el modal de sesion expirada descomentar ==
    //document.querySelector(`#${VALUES.MDLAUTH}`).classList.remove('hide');
    //document.querySelector(`#${VALUES.MDLAUTH}`).open();
    // == Comentar si descomenta la lineas anteriores ==
    callCommunicatorPortal(window.AppConfig.crossChannels.showDialogExpiredSession);
  });

  auhServ().addEventListener('granting-ticket-error', ({detail}) => {
    showLoadingGlobalApp(false);
	  elDOM(`#${VALUES.NOT}`).setAttribute('class', 'noti-danger');
    elDOM(`#${VALUES.NOT}`).innerHTML = `<p class="error-host-service-certificate">Error de acceso al servicio de Autenticación, por favor verificar el siguiente enlace <a href="${appConf().services.host}" target="_blank" >verificar</a></p>`;
    elDOM(`#${VALUES.NOT}`).opened = true;
    console.log('granting-ticket-error', detail);
  });

  auhServ().addEventListener('idp-error', ({detail}) => {
    showLoadingGlobalApp(false);
    console.log('idp-error', detail);
  });

  auhServ().addEventListener('sso-error', () => {
    showLoadingGlobalApp(false);
    console.log('sso-error');
  });

  auhServ().addEventListener('error-request-dp', ({detail}) => {
    showLoadingGlobalApp(false);
    console.log('error-request-dp', detail);
  });

  window.addEventListener('on-global-loading', ({ detail }) => {
    showLoadingGlobalApp(detail);
  });

  elDOM(`#${VALUES.MDLAUTH}`).addEventListener('button-click', () => {
      window.location = appConf().securityConf.externalDMZ;
  });
};

const initializeConfigurations = async () => {
  await awaitAppConf();
  const params = (new URL(document.location)).searchParams;
  let appId = params.get('appId');
  if (appId) {
    console.log('appId', appId);
    window.AppConfig.applicationId = appId;
  }
  elDOM(`#${VALUES.MDLAUTH}`).classList.remove('hide');
  elDOM(`#${VALUES.MDLAUTH}`).linkHref = appConf().securityConf.externalDMZ;
  addListenersApp();
  await applySessionConfg();
};

const addElementsBaseCross = async () => {
  await awaitAppConf();
  const _cmpSubmenusApps = document.querySelector(`#${VALUES.SM}`);
  const _cmpPortal = document.querySelector(`#${VALUES.CON}`);
  _cmpPortal.applicationId = `${appConf().applicationId}`;
  console.log('applicationId=', appConf().applicationId);
  // == Esta opcion es sugerida si se desea manejar las opciones en memoria de la aplicacion == 
  _cmpSubmenusApps.items = appConf().optionsRoutes;
  
  // == Esta opcion es sugerida si se desea manejar las opciones desde los servicios de menurización
  //const _menuComponents = await callCommunicatorPortal(window.AppConfig.crossChannels.getComponents);
  //_cmpSubmenusApps.items = _menuComponents;

  _cmpSubmenusApps.defaultSelectedKey = '1';
};

document.addEventListener('DOMContentLoaded', async () => {
  await addElementsBaseCross();
  await initializeConfigurations();  
});
