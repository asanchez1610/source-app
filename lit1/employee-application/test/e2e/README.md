# What's this project about?

This project aims to be a set of E2E functional tests that go along with the application scaffold, as well as a starting point/boilerplate for your requirements and custom use cases, and a quick guide about how to write functional tests for your Cells Applications.

## What is it made of?

- [Cells Pepino V2](https://platform.bbva.com/en-us/developers/engines/cells/documentation/testing/cells-pepino-v2), a lightweight test runner based on top of WebdriverIO 7, CucumberJS as testing framework, and Selenium/Appium as browser/mobile automation tools.

- [Global E2E Testing Framework for javascript](https://globaldevtools.bbva.com/bitbucket/projects/BGT/repos/e2e-js-framework/browse), a set of tools for developing e2e tests with javascript. It provides well known, battle tested, tools and patterns so you can start coding with zero effort e2e tests.

For more information about these products, how do they works, etc..., please, check the associated links.

## Usage

First of all, you need to install E2E project dependencies - remember to be inside `test\e2e` folder!

```bash
npm i
```

After all dependencies are installed, you only need to execute the test runner from inside `test\e2e` folder!

The application comes with different configurations to run the tests in different scenarios. Also the `test\e2e\package.json` files has scripts to run the tests.

### Running the tests in local

First, go to the root of your project and serve the application in production mode:
```bash
cells app:serve -c dev.js -b
```

Then, open another terminal. Go to the `test\e2e` folder and run:

```bash
npm run test
```

### Running the tests with VBank

To run the tests and record the HTTP responses use:

First, go to the root of your project and serve the application in production mode using the vbank configuration file.
```bash
cells app:serve -c vbank.js -b
```

Then, open another terminal. Go to the `test\e2e` folder and run:

```bash
npm run test:vbank:record
```
   
To run the tests and using the recorded HTTP responses use:

```bash
npm run test:vbank:replay
```

### Running the tests with VBank (legacy with Java 11)

To run the tests and record the HTTP responses use:

First, go to the root of your project and serve the application in production mode using the vbank configuration file.

```bash
cells app:serve -c vbank.js -b
```

Then, open another terminal. Go to the `test\e2e` folder and run:

```bash
npm run test:vbank:record:legacy
```
   
To run the tests and using the recorded HTTP responses use:

```bash
npm run test:vbank:replay:legacy
```

# CI (Jenkins)

You can run your E2E tests in CI, with only extending your current jenkinsfile with the following configuration.

__Remember to configure it properly upon to your needs: (buildConfigs, and e2eTests block).__

```groovy
...
entrypointParams = [
        type            : "cellsApp",
        buildConfigs    : [
                config1:    [config:'dev', build:'vulcanize', e2econfig: ['pepino']] // Your build requirements.
        ],
        deployS3        : true,
        e2eTests        : [
                pepino: [
                        e2econfig: ["browser.js"], // Your custom config file for jenkins environment.
                        abortOnFailure: true,
                        isLocalTest: true,
                        baseFolder: "test/e2e",
                        testRunner: "pepino",
                ],
        ]
]
...
```