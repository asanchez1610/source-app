import { demoAppContainerDarkModeThemeStyles } from '@cells-demo/demo-app-container';
import { demoAppTemplateDarkModeThemeStyles } from '@cells-demo/demo-app-template';

export default {
  ...demoAppContainerDarkModeThemeStyles,
  ...demoAppTemplateDarkModeThemeStyles,
};