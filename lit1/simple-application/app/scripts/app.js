(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: window.AppConfig.routes
  });
}());