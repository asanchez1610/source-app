// Import here your LitElement components (non critical for starting up)
import '@bbva-web-components/bbva-web-template-modal/bbva-web-template-modal.js';
import '@bbva-web-components/bbva-web-notification-toast/bbva-web-notification-toast.js';
import '@bbva-web-components/bbva-foundations-spinner/bbva-foundations-spinner.js';
import '@cells-portals-cross-components/cells-portals-core-submenus-ppgc/cells-portals-core-submenus-ppgc.js';
import '@bbva-web-components/bbva-core-moment-import/lib/bbva-core-moment-import.min.js';
import '@cells-portals-cross-components/cells-standalone-header-apps-ppgc/cells-standalone-header-apps-ppgc.js';
import '@cells-portals-cross-components/cells-portals-core-session-dm-ppgc/cells-portals-core-session-dm-ppgc.js';