import { html, LitElement } from 'lit-element';
import { BbvaCoreIntlMixin as intl } from '@bbva-web-components/bbva-core-intl-mixin';
import { CellsCoreElementMixin as mxnCore } from '@cells-portals-cross-components/cells-portals-core-mixins-ppgc';
import { CellsPageMixin as cellsPage } from '@cells/cells-page-mixin';
import '../../../app/utils/ui/container-app-template.js';
import { BbvaCoreIconset } from '@bbva-web-components/bbva-core-iconset';


const _iconSet = new BbvaCoreIconset({
  name: 'bbva',
  width: 300,
  height: 300
});

export class CellsMasterPage extends intl(mxnCore(cellsPage(LitElement))) {

  static get properties() {
    return {
      appProps: Object,
      applicationParams: Object
    };
  }

  constructor() {
    super();
    this.appProps = window.AppConfig;
    this.host = this.extract(this.appProps, 'services.host', '');
    this.paths = this.extract(this.appProps, 'services.paths', {});
    this.securityConf = this.extract(this.appProps, 'securityConf', {});
    this.eventRequestName = this.extract(this.appProps,'ctts.eventRequestName', 'send-request-dp');
    this.constants = this.extract(this.appProps, 'ctts', {});
    this.applicationParams = {}
    if (!window.isPresentListenerNavigate) {
      window.addEventListener('select-menu-item', ({detail}) => { 
          this.navigate(detail.path); 
      });
      window.isPresentListenerNavigate = true;
    }
  }

  iconBBVA(iconName, options={}) {
    this.buildIcon(iconName, options.width, options.height, _iconSet);
  }

  get glAuthServ () {
    return document.querySelector('#auth-serv-gl-cmp');
  }

  get glSpinner () {
    return document.querySelector('#spinner-gl-cmp');
  }

  get glNotification () {
    return document.querySelector('#notification-gl-cmp');
  }

  get glMdlErrorAuth () {
    return document.querySelector('#modal-error-auth-serv-gl-cmp');
  }

  get glSessionDm () {
    return document.querySelector('#session-dm-gl-cmp');
  }

  get glHeaderApp () {
    return document.querySelector('#header-gl-cmp');
  }

  get glSubMenuApp () {
    return document.querySelector('#menu-gl-cmp');
  }
  
  showToastNoti({ message='', type='info' }) {
    this.glNotification.setAttribute('class', `noti-${type}`);
    this.glNotification.innerHTML = `<p>${message}</p>`;
    this.glNotification.opened = true;
  }

  async initMasterPage() {
    await this.updateComplete;
    this.overwriteOnPageMethods();
  }

  beforeOnPageEnter() {}

  beforeOnPageLeave() {}

  interceptorBeforeMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      fn.apply(object);
      originalMethod.apply(object, arguments);
    };
  }

  interceptorAfterMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      originalMethod.apply(object, arguments);
      fn.call(object);
    };
  }

  overwriteOnPageMethods() {
    if (this.onPageEnter && typeof this.onPageEnter === 'function') {
      this.interceptorBeforeMethod(this, 'onPageEnter', this.beforeOnPageEnter);
    }
    if (this.onPageLeave && typeof this.onPageLeave === 'function') {
      this.interceptorBeforeMethod(this, 'onPageLeave', this.beforeOnPageLeave);
    }
  }

  awaitTsecPresent() {
    let clear;
    let count = 0;
    const _tsec = window.sessionStorage.getItem('tsec');
    return new Promise((resolve, reject) => {
      clear = setInterval(() => {
        count = count + 1;
        if (_tsec) {
          resolve(true);
          clearTimeout(clear);
        } else if (count > 80) {
          reject(false);
        }
      }, 500);
    });
  }

  content(contentHtml, titlePage='') {
    return html` <container-app-template page-title="${titlePage}">
      <div class="container-page" slot="app-main-content">
          ${contentHtml}
      </div>   
    </container-app-template>`;
  }
}
