import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
main .container-main h1 {
  font-weight: lighter;
  font-size: 1.65em;
  padding-bottom: 5px;
  border-bottom: 1px solid #e1e1e1;
}
main .container-main p {
  font-size: 1.2em;
  line-height: 1.5em;
  text-align: justify;
}
main .container-main a {
  font-size: 1.4em;
  font-weight: normal;
  color: #1464A5;
  text-decoration: none;
}
main .container-main small {
  color: #2E4452;
}
main .container-main .description-component {
  margin-top: 5px;
}
`;
