import { html } from 'lit-element';
import { CellsMasterPage } from '../../../app/utils/ui/CellsMasterPage.js';
import styles from './main-page.css.js';
import sharedStyles from '../../../app/pages/shared-styles/pages-styles.css.js';

class MainPage extends CellsMasterPage {
  static get is() {
    return 'main-page';
  }

  static get properties() {
    return {};
  }

  static get styles() {
    return [styles, sharedStyles];
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  onPageEnter() {
    this.testInitialAction();
  }

  /* Ejemplo de validacion de invoacion de servicio considerando la existencia del TSEC */
  async testInitialAction() {
    const _presentTsec = await this.awaitTsecPresent();
    if (_presentTsec) {
      console.log('existe TSEC puede invocar servicios !');
    }
  }

  /* eslint-disable lit-a11y/alt-text */
  /* eslint-disable lit/attribute-value-entities */
  render() {
    return this.content(
      html`
        <main>
          <div class="container-main">
            <h1>¡Bienvenido! 🧑‍💻</h1>
            <p>El presente proyecto ha sido diseñado mediante el consenso de diversos desarrolladores, cada uno contribuyendo con su experiencia para la creación de una serie de componentes transversales. Estos componentes están destinados a minimizar la duplicación de esfuerzos en los proyectos que utilizan la tecnología <strong>Cells</strong></p>
            <h1>Componentes Trasversales</h1>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-standalone-header-apps-ppgc/master/cellsLitComponent/1.0.0/demo/index.html?selected=0&view=visual" target="_blank" >🌀 cells-standalone-header-apps-ppgc</a>
            <p class="description-component">Componente Header para aplicaciones standalone.
            <br><small>@autor: Arquitectura Canales</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-submenus-ppgc/master/cellsLitComponent/1.0.0/demo/index.html" target="_blank" >🌀 cells-portals-core-submenus-ppgc</a>
            <p class="description-component">Componente de menu horizontal de sub aplicaciones.
            <br><small>@autor: Frans Paico Reyes (frans.paico.contractor@bbva.com)</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-datatable-ppgc/master/cellsLitComponent/0.5.0/demo/index.html?resolution=largeDesktop&panel=demo" target="_blank" >🌀 cells-portals-core-datatable-ppgc</a>
            <p class="description-component">Componente de menu horizontal de sub aplicaciones.
            <br><small>@autor: Esli Samuel Marquez Zavaleta (esli.marquez.contractor@bbva.com)</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-auth-service-ppgc/master/cellsLitComponent/1.0.0/demo/index.html?selected=0&panel=demo" target="_blank" >🌀 cells-portals-core-auth-service-ppgc</a>
            <p class="description-component">Componente que se encarga de controlar los eventos exitosos o fallidos de la autenticacion de empleados. Centraliza las peticiones HttpRequest y realiza la logica de refresco de TSEC cuando este ha caducado. Permite la comunicación con servicios que requieran lectura o envio de archivos.
            <br><small>@autor: Jonathan Diaz Castillo (jonathan.diaz.castillo@bbva.com)</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-mixins-ppgc/master/cellsLitComponent/1.0.0/demo/index.html" target="_blank" >🌀 cells-portals-core-mixins-ppgc</a>
            <p class="description-component">Clase Padre o Mixing que comparte funciones utilitarias para los componentes.
            <br><small>@autor: Arquitectura Canales</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-session-dm-ppgc/master/cellsLitComponent/1.0.0/demo/index.html" target="_blank" >🌀 cells-portals-core-session-dm-ppgc</a>
            <p class="description-component">Componente encargado de gestionar y estructurar la información de los servicios de usuario, aplicaciones y permisos.
            <br><small>@autor: Arquitectura Canales</small>
            </p>
            <a href="https://au-bbva-andromeda.appspot.com/api/projects/au-bbva-cells-platform/segments/au-bbva-cells-artefacts/resources/cells/apps/pe_ppgc_app-id-1268428_dsg/cells-portals-core-menu-cards-ppgc/master/cellsLitComponent/1.0.0/demo/index.html?resolution=largeDesktop" target="_blank" >🌀 cells-portals-core-menu-cards-ppgc</a>
            <p class="description-component">Componente para el manejo de el menu de cards de aplicaciones.
            <br><small>@autor: Arquitectura Canales</small>
            </p>
          </div>
        </main>
      `,
      '{{applicationTitle}}'
    );
  }
}
window.customElements.define(MainPage.is, MainPage);
