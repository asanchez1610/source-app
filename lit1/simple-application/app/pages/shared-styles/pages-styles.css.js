import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

.container-page {
  width: 100%;
  max-width: 100%;
  height: 100vh;
  background-color: #E9E9E9;
  overflow: hidden;
  overflow-y: auto;
}

bbva-foundations-spinner {
  background-color: #004481;
  width: 100%;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
}

.hide {
  display: none;
}

main {
  margin-top: 60px;
  background-color: #E9E9E9;
  display: flex;
  min-height: calc(100vh - 55px);
}
main .container-main {
  background-color: white;
  padding: 15px 25px;
  width: calc(100vw - 105px);
  margin: 25px;
  margin-top: 25px;
  border: 1px solid rgba(165, 165, 165, 0.75);
  box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  -webkit-box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  -moz-box-shadow: 4px 4px 5px 0px rgba(165, 165, 165, 0.75);
  overflow: auto;
}
`;
