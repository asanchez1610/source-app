import { html } from 'lit-element';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-form-textarea/bbva-web-form-textarea.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
import { CellsMasterPage } from '../../../app/utils/ui/CellsMasterPage.js';
import styles from './test-service-page.css.js';
import sharedStyles from '../../../app/pages/shared-styles/pages-styles.css.js';

class TestServicePage extends CellsMasterPage {
  static get is() {
    return 'test-service-page';
  }

  static get properties() {
    return {};
  }

  static get styles() {
    return [styles, sharedStyles];
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  async testService() {
    const settings = {
      path: this.shadowRoot.querySelector('#path').value,
      method: this.shadowRoot.querySelector('#method').value,
      host: this.shadowRoot.querySelector('#host').value
    };
    this.appLoading(true);
    const body = this.shadowRoot.querySelector('#body').value;
    if (body) {
      settings.body = body;
    }
    try {
      const response = await this.requestAwait(settings);
      this.appLoading(false);
      this.customer = response.detail;
      this.shadowRoot.querySelector('#text-result').value = JSON.stringify(
        response.detail,
        null,
        2
      );
    } catch (e) {
      console.log(e);
      this.appLoading(false);
      this.shadowRoot.querySelector('#text-result').value = JSON.stringify(
        {},
        null,
        2
      );
    }

  }


  render() {
    return this.content(html`
    <main>
    <div class="container-main" >
          <section class="form">
            <h2>Pruebas de servicios</h2>

            <bbva-web-form-text
              id="host"
              value="${this.extract(this.appProps, 'services.host', '')}"
              label="Host"
            ></bbva-web-form-text>
            <bbva-web-form-text
              id="path"
              value="${this.extract(this.appProps, 'services.paths.security.listBusinessAgents', '')}"
              label="Path"
            ></bbva-web-form-text>
            <bbva-web-form-select id="method" label="Método">
              <bbva-web-form-option selected="selected" value="GET"
                >GET</bbva-web-form-option
              >
              <bbva-web-form-option value="POST">POST</bbva-web-form-option>
              <bbva-web-form-option value="PUT">PUT</bbva-web-form-option>
              <bbva-web-form-option value="DELETE">DELETE</bbva-web-form-option>
              <bbva-web-form-option value="PATCH">PATCH</bbva-web-form-option>
            </bbva-web-form-select>
            <bbva-web-form-textarea
              id="body"
              label="Body"
            ></bbva-web-form-textarea>

            <bbva-web-button-default @click="${this.testService}"
              >Ejecutar</bbva-web-button-default
            >
          </section>

          <section class="response">
            <textarea id="text-result" readonly=""></textarea>
          </section>
  </div>
        </main>

    `, 'Test de servicios');
  }
}
window.customElements.define(TestServicePage.is, TestServicePage);