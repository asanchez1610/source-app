<!doctype html>

<html lang="en-US">

<head>
  <meta charset="utf-8">
  <meta name="description" content="Example App">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{applicationTitle}}</title>
 <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/icon-bbva192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/icon-bbva144x144.png">

  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed">

  <link rel="stylesheet" href="styles/main.css">
  <script>
    window.IntlMsg = window.IntlMsg || {};
    window.IntlMsg.lang = document.documentElement.lang;
  </script>
</head>

<body class="loading">
  
  <!-- splash application preview -->
  <div id="splash" class="splash">
    
    <div class="content-present-app wrapper hide" >
    
      <img src="resources/logo.svg" height="48" />

    </div>

    <div class="simple-splash" >
      <div class="loadingio-spinner-eclipse-6pc932zv7vn"><div class="ldio-mtovv4h8tpf">
      <div></div>
      </div></div>
    </div>

  </div>
  
 <!-- Content aplication -->
  <div id="app__content"></div>

  <!-- cross container for all pages -->
  <div id="cells-template-__cross">
  
    <cells-standalone-header-apps-ppgc titleApp="App Demo" show-buttons="" id="header-gl-cmp" >
    
    <div class="menu-horizontal" slot="header-aditional-brand">

      <cells-portals-core-submenus-ppgc id="menu-gl-cmp" class="sub-menu-cmp"></cells-portals-core-submenus-ppgc>

    </div>

    </cells-standalone-header-apps-ppgc>
	  
    <cells-portals-core-auth-service-ppgc id="auth-serv-gl-cmp"></cells-portals-core-auth-service-ppgc>

	  <bbva-foundations-spinner with-mask="" size="120" id="spinner-gl-cmp"></bbva-foundations-spinner>

    <cells-portals-core-session-dm-ppgc id="session-dm-gl-cmp"></cells-portals-core-session-dm-ppgc>

	  <bbva-web-notification-toast id="notification-gl-cmp"></bbva-web-notification-toast>
	  <bbva-web-template-modal 
		  id="modal-error-auth-serv-gl-cmp" 
		  help=""
		  heading="Problemas de autenticación"
		  button="Acceder con  credenciales"
		   >
		   
		   <img slot="help-micro" src="resources/images/security_warning.svg" width="95" />
				<p>
				  Ha ocurrido un problema al verificar sus credenciales, por favor
				  vuelva a intentarlo o valide el login de acceso.
				</p>
	  </bbva-web-template-modal>
  
  </div>

  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>

  <script src="vendor/bowser.min.js"></script>
  <script type="module" src="scripts/app-module.js"></script>
  <script src="resources/js/index.tpl.js"></script>
</body>

</html>
