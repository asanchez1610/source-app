const objCtts = require('./constants.js');
const objPaths = require('./paths.js');
const objRoutes = require('./routes.js');

const appConfig = {
  /**
   * Properties used to control settings of Cells Bridge and the build process
   */
  cells_properties: {
    enableLitElement: true,
    onlyLitElements: true,
    initialTemplate: 'main',
    transpile: true,
    transpileExclude: ['webcomponentsjs', 'moment', 'd3', 'bgadp*'],

    debug: true,
    logs: false,

    /**
     * Relative path to folder that contains dynamic pages (.json files)
     */
    templatesPath: './dynamicPages/',
    /**
     * Relative path to folder that contains static pages (.js files)
     */
    pagesPath: './pages/',
    prplLevel: 1,
    initialBundle: [ 'main' ],

    /* Internationalization options */
    locales: {
      languages: [],
      intlInputFileNames: [ 'locales' ],
      intlFileName: 'locales',
    }
  },

  /**
   * These properties are specific to your application.
   * Here you can use your own properties, so it is an
   * open set of properties that you can use at your
   * convenience.
   * These variables will be available in your
   * application ins the window.AppConfig object
   */
  app_properties: {
    mock: true,
    applicationId: '{{applicationId}}',
    services: {
      paths: objPaths.paths()
    },
    securityConf: {
      aapId: '{{aapId}}',
      pathGT: 'TechArchitecture/pe/grantingTicket/V02'
    },
    ctts: objCtts.ctts(),
    routes: objRoutes.routes()
  }
};

module.exports = appConfig;
