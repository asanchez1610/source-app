const appConfig = require('./sources/props.js');
const envConf =  { ...appConfig };

envConf.app_properties.services.host = 'https://dev-arqaso.work.pe.nextgen.igrupobbva:8050';
//envConf.app_properties.services.host = 'https://tst-glomo.bbva.pe/SRVS_A02';
envConf.app_properties.securityConf.externalDMZ = 'https://ei-community.grupobbva.com/';
module.exports = envConf;
