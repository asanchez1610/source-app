const appConfig = require('./sources/props.js');
const envConf =  { ...appConfig };

envConf.app_properties.services.host = 'https://aso-aaii-pe.live-02.nextgen.igrupobbva';
envConf.app_properties.securityConf.externalDMZ = 'https://branches-i-pe.live-04.platform.bbva.com/index.html';
module.exports = envConf;
