const configBase = require('./base.conf');
const recording = process.env.VBANK_RECORDING || false;

if (recording) {
  console.log('Recording mode enabled');
}

exports.config = {
  ...configBase.config,
  vbank: {
    timeout: 100 * 1000,
    verbose: true,
    workspace: 'vbank/records',
    backends: [
      {
        name: 'mock',
        realUrl: recording && 'http://localhost:8001',
        cors: {
          headers: ['*'],
        },
        port: '7071',
      },
    ],
    recording,
    parallel: false,
    logFile: 'vbank.log',
  },
};
