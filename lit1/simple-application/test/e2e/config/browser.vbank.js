const { configBuild } = require('@testing/wdio-config');
const { config } = require('./base.vbank.conf');
const { AxeService } = require('@wdio/axe-service');

const chrome = (exports.chrome = {
  browserName: 'chrome',
  'goog:chromeOptions': {
    args: [
      '--disable-dev-shm-usage',
      '--disable-search-engine-choice-screen',
      '--headless',
      '--no-sandbox',
      '--window-size=1366,1068',
    ],
  },
});
const firefox = (exports.firefox = { browserName: 'firefox' });
const edge = (exports.edge = { browserName: 'MicrosoftEdge' });
const safari = (exports.safari = { browserName: 'safari' });

const all = (exports.all = [chrome]);

exports.config = configBuild(config, {
  capabilities: all,
  axe: {},
  services: [[AxeService, {}]],
});
