const appConfig = require('./sources/props.js');
const envConf = { ...appConfig };
const _functions = require('./sources/functions.js');
const _envProps = require('./sources/env-props/env-props-live.json');
const { remoteDataMenu } = envConf.app_properties.ctts.settingsConf;

envConf.app_properties.services.host = 'https://aso-aaii-pe.live-02.nextgen.igrupobbva';
envConf.app_properties.securityConf.externalDMZ = 'https://branches-i-pe.live-04.platform.bbva.com/portal/index.html';
envConf.app_properties.applicationsItems = _functions.mapperApplicationsItems(envConf.app_properties.applicationsItems, _envProps.applicationsItems, remoteDataMenu);
envConf.app_properties.appItemsMapper = _envProps;

module.exports = envConf;

