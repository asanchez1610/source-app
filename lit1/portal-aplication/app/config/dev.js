const appConfig = require('./sources/props.js');
const envConf = { ...appConfig };
const _functions = require('./sources/functions.js');
const _envProps = require('./sources/env-props/env-props-dev.json');
const { remoteDataMenu } = envConf.app_properties.ctts.settingsConf;

envConf.app_properties.services.host = 'https://dev-arqaso.work.pe.nextgen.igrupobbva:8050';
//envConf.app_properties.services.host = 'https://tst-glomo.bbva.pe/SRVS_A02';
envConf.app_properties.securityConf.externalDMZ = 'https://ei-community.grupobbva.com/';
envConf.app_properties.applicationsItems = _functions.mapperApplicationsItems(envConf.app_properties.applicationsItems, _envProps.applicationsItems, remoteDataMenu);
envConf.app_properties.appItemsMapper = _envProps;

module.exports = envConf;