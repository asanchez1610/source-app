const crossChannels = () => {
    return {
      readyChildren: 'on-core-portal-ready-child',
      userSession: 'on-core-portal-get-data-user',
      tsec: 'on-core-portal-get-data-tsec', 
      listApps: 'on-core-portal-get-data-apps',
      getComponents: 'on-core-portal-get-components',
      childDataParams: 'on-core-portal-get-params-child',
      addTab: 'on-core-portal-add-tab',
      loadUserSession: 'on-user-session-load',
      sendNotification: 'on-notification-toast',
      showDialogExpiredSession: 'on-show-dialog-expired-session'
    };
  };
  
  module.exports = {
    crossChannels: crossChannels
  };