const paths = () => {
  return {
    security: {
      picIdm: 'idm/v1/users?userId',
      listBusinessAgents: 'business-agents/v0/business-agents?expand=applications,contact-details&id',
      getApplicationsByRoles: 'user-settings/v0/applications/search',
      getComponentByApplication: 'user-settings/v0/applications/{applicationId}/components/search'
    }
  };
};

module.exports = {
  paths: paths,
};
