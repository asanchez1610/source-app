const mapperApplicationsItems = (items, addProps = [], remoteDataMenu) => {
    if (remoteDataMenu) {
        return items;
    }
    if (!items) {
        return [];
    }
    return items.map(_item => {
        let _mapRecord = { ..._item };
        addProps.forEach(_prop => {
            if (`${_prop.id}` === `${_item.id}` && _prop.data) {
                _mapRecord = { ..._mapRecord, ..._prop.data };
            }
        });
        return _mapRecord;
    });
}

module.exports = {
    mapperApplicationsItems: mapperApplicationsItems
};
  