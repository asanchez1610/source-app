const ctts = () => {
  return {
    eventRequestName: 'send-request-dp',
    settingsConf: {
      remoteDataMenu: true,
      remoteDataComponents: true
    },
    portalProperties: {
      applicationPortalId: '{{applicationPortalId}}',
      principalPageTitle: '{{portalTitle}}',
      portalName: '{{portalName}}',
      iconTabPrincipal: '{{iconTabPrincipal}}'
    }
  };
};

module.exports = {
  ctts: ctts
};
