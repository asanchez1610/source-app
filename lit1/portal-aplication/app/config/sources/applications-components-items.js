const appsComponentsItems = () => {
  return [
    {
      id: 3,
      name: 'MENU_1',
      label: 'Opción 01',
      iconName: 'home',
      path: '/home',
      order: 1,
      applicationId: '2',
      rols: ['SIGD043_0002'],
      componentType: {
        id: 1,
        name: 'MENU',
      },
    },
  ];
};

module.exports = {
  appsComponentsItems: appsComponentsItems,
};
