const objCtts = require('./constants.js');
const objPaths = require('./paths.js');
const objRoutes = require('./routes.js');
const objCrossChannels = require('./crossChannels.js');
const objAppsItems = require('./applications-items.js');
const objAppsComponentesItems = require('./applications-components-items.js');

const appConfig = {
  /**
   * Properties used to control settings of Cells Bridge and the build process
   */
  cells_properties: {
    enableLitElement: true,
    onlyLitElements: true,
    initialTemplate: 'main',
    transpile: true,
    transpileExclude: ['webcomponentsjs', 'moment', 'd3', 'bgadp*'],

    debug: true,
    logs: false,

    /**
     * Relative path to folder that contains dynamic pages (.json files)
     */
    templatesPath: './dynamicPages/',
    /**
     * Relative path to folder that contains static pages (.js files)
     */
    pagesPath: './pages/',
    prplLevel: 1,
    initialBundle: [ 'main' ],

    /* Internationalization options */
    locales: {
      languages: [],
      intlInputFileNames: [ 'locales' ],
      intlFileName: 'locales',
    }
  },

  /**
   * These properties are specific to your application.
   * Here you can use your own properties, so it is an
   * open set of properties that you can use at your
   * convenience.
   * These variables will be available in your
   * application ins the window.AppConfig object
   */
  app_properties: {
    mock: true,
    /** Si el valor es lba, consultará al servicio de listBusinessAgents **/
    dataUserType: 'lba',
    /** Nombre del ítem de session storage para el TSEC **/
	  tsecNameSession: 'tsec-portal',
     /** Paths de servicios **/
    services: {
      paths: objPaths.paths()
    },
     /** Parámetros de seguridad **/
    securityConf: {
      aapId: '{{aapId}}',
      pathGT: 'TechArchitecture/pe/grantingTicket/V02',
      enableUserCrypto: true,
      cryptoKey: 'analispectemp-pe-fpextff1-do',
      codecFormat: 'PLAIN'
    },
    /** Atributos constantes **/
    ctts: objCtts.ctts(),
    /** Rutas de páginas de la aplicacón **/
    routes: objRoutes.routes(),
    /** Lista de los canales de comunicacón de potsmessage **/
    crossChannels: objCrossChannels.crossChannels(),
    /** Lista de sub aplicaciones estáticas **/
    applicationsItems: objAppsItems.appsItems(),
    /** Lista de componentes de aplicaciones estáticas **/
    applicationsComponentsItems: objAppsComponentesItems.appsComponentsItems()
  }
};

module.exports = appConfig;
