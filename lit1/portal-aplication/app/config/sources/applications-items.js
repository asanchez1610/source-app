const appsItems = () => {
  return [
    {
      id: 2,
      name: 'Aplicación Demo',
      description:
        'Sub Aplicación de ejemplo para portal de aplicaciones.',
      iconName: 'reporting',
      path: 'http://localhost:8000/dist/',
      groupAppName: 'DEMOS',
      target: 'S',
      rols: ['SIGD043_0002'],
      applicationPortalId: 1
    }
  ];
};

module.exports = {
  appsItems: appsItems,
};

