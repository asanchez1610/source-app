const appConfig = require('./sources/props.js');
const envConf = { ...appConfig };
const _functions = require('./sources/functions.js');
const _envProps = require('./sources/env-props/env-props-mock.json');
const { remoteDataMenu } = envConf.app_properties.ctts.settingsConf;

envConf.app_properties.services.host = 'http://localhost:3000';
envConf.app_properties.enableVBank = true;
envConf.app_properties.securityConf.externalDMZ = 'https://ei-community.grupobbva.com/';
envConf.app_properties.applicationsItems = _functions.mapperApplicationsItems(envConf.app_properties.applicationsItems, _envProps.applicationsItems, remoteDataMenu);
envConf.app_properties.appItemsMapper = _envProps;

module.exports = envConf;

