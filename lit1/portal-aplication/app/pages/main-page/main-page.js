import { html } from 'lit-element';
import { CellsMasterPage } from '../../../app/utils/ui/CellsMasterPage.js';
import { findComponent } from '../../../app/utils/shared/portal-components.js';
import '@cells-portals-cross-components/cells-portals-container-apps-ppgc/cells-portals-container-apps-ppgc.js';
import styles from './main-page.css.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-core-icon/bbva-core-icon.js';
import '@cells-portals-cross-components/cells-portals-core-menu-cards-ppgc/cells-portals-core-menu-cards-ppgc.js';

class MainPage extends CellsMasterPage {
  static get is() {
    return 'main-page';
  }

  static get properties() {
    return {
      /**
      * Aplicaciones del portal
      */
      applications: { type: Array },
      /**
      * Componentes de aplicaciones del portal.
      */
      applicationsComponentes: { type: Array },
      /**
      * Valor de estado de carga de informacion de usuario.
      */
      loadingInfoUser: { type: String },
      /**
      * Valor de estado de carga de items de aplicaciones.
      */
      loadingMenuApps: { type: String },
      /**
      * Valor actual del perfil seleccionado.
      */
      currentRolesId: { type: Array }
    };
  }

  /**
  * Estilos de la página.
  */
  static get styles() {
    return [ styles ];
  }

  constructor() {
    // === Inicialización de variables ===
    super();
    this.applications = [];
    this.currentRolesId = [];
    this.loadingInfoUser = '1';
    this.loadingMenuApps = '1';

    // === Evento de carga de los datos de usuario de sesion  ===
    window.addEventListener(this.channels.loadUserSession, ({ detail }) => this.responseDataInfo(detail));

    // === Evento que evalua la creacion de un nuevo tab en el portal de forma personalizada  ===
    window.addEventListener(this.channels.addTab, ({ detail }) => this.addTabCustom(detail.tab, detail.dataParams));

    // === Evento que oculta los paneles de informacion del portal  ===
    window.addEventListener('click', () => this.cleanView());

    // === Evento que propaga un mensaje a todas las aplicaciones hijas  ===
    this.addEventListener('on-log-activity-portal', ({detail}) => {
      const _arg = {
        eventName: 'on-core-portal-publish-hello-children',
        msg: detail.msg
      };
      this.propagateChildrenEvent(_arg);
    });
  }

  /**
   * metodo que oculta los paneles informativos en general.
   */
  cleanView() {
    this.portal.closeMenuPrincipal();
    this.portal.closeInfoUser();
  }

  /**
   * metodo que asigna los valores de usuario de sesion.
   * @param {Object} userData - Informacion del usuario de sesión.
   */
  async responseDataInfo(userData) {
    if (!userData.id) {
      await this.sleepIterator(500);
      this.loadingMenuApps = null;
      this.showToastNoti({
        message: `No se ha podido obtener información del usuario.${
          userData.messageError ? `[${userData.messageError}]` : ''
        }`,
        type: 'warning',
      });
    }
    this.portal.userProfile = userData;
    this.loadingInfoUser = null;
  }

  /**
   * metodo que se ejecuta al terminar de agregarse un tab.
   * @param {Object} detail - Informacion del tab.
   */
  onCompleteTabAdd({ detail }) {
    const { id, slotName } = detail;
    const _slot = this.portal.shadowRoot.querySelector(
      `slot[name="${slotName}"]`
    );
    const _tpl = document.createElement('div');
    const _cmp = findComponent(id);
    if (_cmp) {
      _tpl.innerHTML = _cmp.content({ nameUser: 'BBVA' });
    } else {
      _tpl.innerHTML = 'Componente no encontrado';
    }
    _slot.appendChild(_tpl);
  }

  /**
   * metodo que se ejecuta al realizar algun evento de cambio de perfil del usuario.
   * @param {Object} detail - Informacion de los perfiles de usuario.
   */
  async onChangeProfileUser({ detail }) {
    // === Limpiar panels informativos  ===
    this.portal.closeAllTabs();
    this.portal.closeInfoUser();

    // === Obtener el valor que indica si los items de menu se obtienen en memoria o por servicios  ===
    const dataRemoteMenuApps = this.extract(this.constants, 'settingsConf.remoteDataMenu', true);

    // === Obtener el valor de el identificador unico del portal ===
    const applicationPortalId = this.extract(this.constants, 'portalProperties.applicationPortalId', this.randomID());

    // === Setear el primer valor de item de portal el cual es fijo ===
    const firstApplication = [
      {
        id: applicationPortalId,
        name: this.extract(this.constants, 'portalProperties.portalName', ''),
        label: this.extract(this.constants, 'portalProperties.principalPageTitle', ''),
        tooltip: this.extract(this.constants, 'portalProperties.principalPageTitle', ''),
        iconName: this.extract(this.constants, 'portalProperties.iconTabPrincipal', null),
        order: -1,
        componentType: {
          id: 1,
          name: 'component',
        },
      },
    ];

    // === Obtener los perfiles del usuario de sesion ===
    let rolesId;
    if (this.isEmpty(detail.profileId)) {
      rolesId = this.extract(detail, 'userProfile.appProfiles', []).map((_item) => _item.code);
    } else {
      rolesId = [ detail.profileId ];
    }
    this.currentRolesId = rolesId;
    let listApplications;

    // === Bifurcacion de logica para la visualizacion de aplicaciones en memoria o desde servicio ===
    if (dataRemoteMenuApps) {
      // === Logica de informacion desde servicios ===
      const keyPath = 'getApplicationsByRoles';
      this.loadingMenuApps = '1';
      this.glDmSession.paths[keyPath] = this.extract(this.paths, `security.${keyPath}`, '');
      await this.requestUpdate();
      const body = {
        rolesId,
        applicationPortalId,
      };
      try {
        listApplications = await this.glDmSession.findAplicationsAndComponentesByProfile({
          keyPath,
          body,
        });
        if (this.appProps.appItemsMapper && this.appProps.appItemsMapper.applicationsItems && this.appProps.appItemsMapper.applicationsItems.length > 0) {
          listApplications = listApplications.map(_item => {
            let _mapRecord = { ..._item };
            this.appProps.appItemsMapper.applicationsItems.forEach(_prop => {
              if (`${_prop.id}` === `${_item.id}` && _prop.data) {
                _mapRecord = { ..._mapRecord, ..._prop.data };
              }
            });
            return _mapRecord;
          });
        }
        this.applications = [...firstApplication, ...listApplications];
        this.menuCards.items = listApplications;
        this.loadingMenuApps = null;
      } catch (e) {
        this.showToastNoti({message: 'Ha ocurrido un error al obtener la lista de aplicaciones.', type: 'warning'});
        this.loadingMenuApps = null;
      }

    } else {
      // === Logica de informacion desde memoria ===
      this.loadingMenuApps = '1';
      await this.sleepIterator(200);
      let _applicationsItems = this.extract(this.appProps, 'applicationsItems', []);
      listApplications = await this.glDmSession.findItemsMenuLocalByProfile(
        rolesId,
        applicationPortalId,
        _applicationsItems
      );
      this.applications = [...firstApplication, ...listApplications];
      this.menuCards.items = listApplications;
      this.loadingMenuApps = null;
    }
  }

  /**
   * Metodo que agrega un nuevo tab
   * @param {String} label - Etiqueta del tab.
   * @param {String} path - path o url que corresponde al tab.
   * @param {String} iconName - Icono del tab.
   * @param {String} id - Identificador del tab.
   * @param {Object} data - datos adicionales que proporciona el tab.
   */
  addTabCustom({ label, path, iconName, id }, data) {
    const _id = id;
    const _basedata = this.generateDataTab(_id);
    _basedata.order = 1001;
    const _tab = { label, path, iconName, ..._basedata };
    _tab.data = data;
    this.portal.addItemTab(_tab);
  }

  /**
   * Metodo que se ejecuta al seleccionar un item de aplicacion
   * @param {Object} item - Información del tab.
   */
  async itemCardSelected(item) {
    const { detail: data = {} } = item;
    if (!data.id || !data.path) {
      return;
    }
    this.dispatch('on-log-activity-portal', { msg: `Se ha seleccionado la aplicación ${data.name}`});
    data.label = data.name;
    try {
      const _detail = await this.getAplicactionsComponents(data);
      this.portal.addItemTab(_detail);
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Metodo que se ejecuta al seleccionar un item de aplicacion
   * @param {Object} detail - Información del item de aplicación.
   * @returns {Array} lista de componentes pertenecientes a la aplicación.
   *
   */
  async getAplicactionsComponents(detail) {
    // === Si el orden es negativo, es el tab por defecto, no aplica la logica ===
    if (detail.order < 0) {
      return detail;
    }

    // === Si la informacion es indefinida o nula se le asigna un valor vacio por defecto ===
    if (!detail.data) {
      detail.data = {};
    }

    // === Bifurcacion de logica para obtener la informacion por memoria o por servicios ===
    const dataRemoteMenuApps = this.extract(this.constants, 'settingsConf.remoteDataComponents', true);
    let rols = this.currentRolesId;
    if (!dataRemoteMenuApps) {
      // === Logica de informacion desde memoria ===
      this.appLoading(true);
      await this.sleepIterator(200);
      const _applicationsComponents = this.extract(this.appProps, 'applicationsComponentsItems', []);
      this.applicationsComponentes = await this.glDmSession.findItemsComponentsLocalByProfile(
        rols,
        detail.id,
        _applicationsComponents
      );
      await this.updateComplete;
      console.log('this.applicationsComponentes', this.applicationsComponentes);
      detail.data.components = this.applicationsComponentes;
      this.appLoading(false);
      return detail;
    }
    this.appLoading(true);

    // === Logica de informacion desde servicios ===
    const keyPath = 'getComponentByApplication';
    let _path = this.extract(this.paths, `security.${keyPath}`, '');
    _path = _path.replace('{applicationId}', detail.id);
    this.glDmSession.paths[keyPath] = _path;
    const body = {
      rolesId: this.currentRolesId,
    };
    try {
      this.applicationsComponentes = await this.glDmSession.findAplicationsAndComponentesByProfile({
        keyPath,
        body,
      });
      await this.updateComplete;
      detail.data.components = this.applicationsComponentes;
      this.appLoading(false);
    } catch (e) {
      this.showToastNoti({message: 'Ha ocurrido un error al obtener los componentes de la aplicación.', type: 'warning'});
      this.appLoading(false);
      detail.data.components = [];
    }

    return detail;
  }

  /**
   * Metodo que se ejecuta al seleccionar un item de aplicacion del menu vertical
   * @param {Object} detail - Información del item de aplicación.
   */
  async onSelectionAppMenuItem({ detail }) {
    console.log('onSelectionAppMenuItem', detail);
    try {
      const _deatil = await this.getAplicactionsComponents(detail);
      this.portal.addItemTab(_deatil);
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Metodo que obtiene el componente de menu cards de aplicaciones
   * @returns {Object} menuCards - Componente cells-portals-core-menu-cards-ppgc.
   */
  get menuCards() {
    return this.element('cells-portals-core-menu-cards-ppgc');
  }

  /* eslint-disable lit-a11y/alt-text */
  render() {
    return this.content(
      html`
        <cells-portals-container-apps-ppgc
          id="portal-container"
          urlLogoBBVA="resources/images/logo_bbva_blanco.svg"
          urlUserIcon="resources/images/user.svg"
          urlUserDetailIcon="resources/images/user-detail-icon.svg"
          titleTabPrincipal="${this.extract(this.constants, 'portalProperties.principalPageTitle', 'Página Principal')}"
          .applications="${this.applications}"
          .loadingDataProfile="${this.loadingInfoUser}"
          .loadingDataMenu="${this.loadingMenuApps}"
          iconTabPrincipal="${this.extract(this.constants, 'portalProperties.iconTabPrincipal', null)}"
          hide-menu-apps=""
          @on-comple-tab-add="${this.onCompleteTabAdd}"
          @on-change-profile-user="${this.onChangeProfileUser}"
          @on-selection-app-menu-item="${this.onSelectionAppMenuItem}"
        >
          <main class="wrapper-content" slot="content-principal">
            <cells-portals-core-menu-cards-ppgc
              .loadingListApps="${!!this.loadingMenuApps}"
              @on-selected-menu-item-card="${(evt) => this.itemCardSelected(evt)}"
              titleWelcome="Bienvenido al Portal de aplicaciones"
              textAppsNotFound="No se encontraron resultados."
              textAppEmpty="Tu perfil no cuenta con aplicaciones asignadas para el portal en estos momentos."
            ></cells-portals-core-menu-cards-ppgc>
          </main>
        </cells-portals-container-apps-ppgc>
      `,
      '{{applicationTitle}}'
    );
  }
}
window.customElements.define(MainPage.is, MainPage);
