import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`
.container-page {
  width: 100%;
  max-width: 100%;
  height: 100vh;
  background-color: #fff;
}

container-app-template {
  overflow: hidden;
  height: 100vh;
  width: 100vw;
}

.wrapper-content {
  height: calc(100vh - 64px);
  width: 100vw;
  overflow: auto;
  background: #fff;
  overflow-x: hidden;
}

cells-portals-core-menu-cards-ppgc {
  --bg-card__overlay: #f4f4f4;
  --bg-header-cards-filter: #f4f4f4;
  --height-card: 350px;
  --width-card: 380px;
  padding-bottom: 20px;
}
`;
