/**
 * List Componentes in Local Portal
 * Example:
 * {
 *   id: 2,
 *   name: 'Exameple component',
 *   content: ({nameUser='world'}) => {
 *     return `<example-component nameUser="${nameUser}" ></example-component>`;
 *   }
 *  }
 */
export const PortalComponents = [];

export const findComponent = (id) => PortalComponents.filter((item) => item.id === id)[0];
