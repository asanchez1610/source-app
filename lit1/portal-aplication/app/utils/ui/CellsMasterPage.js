import { html, LitElement } from 'lit-element';
import { BbvaCoreIntlMixin as intl } from '@bbva-web-components/bbva-core-intl-mixin';
import { CellsCoreElementMixin as mxnCore } from '@cells-portals-cross-components/cells-portals-core-mixins-ppgc';
import { CellsPageMixin as cellsPage } from '@cells/cells-page-mixin';
import '../../../app/utils/ui/container-app-template.js';

export class CellsMasterPage extends intl(mxnCore(cellsPage(LitElement))) {
  static get properties() {
    return {
      appProps: Object,
      appIdsregister: Array,
      channels: Object,
    };
  }

  constructor() {
    super();
    this.appIdsregister = [];
    this.appProps = window.AppConfig;
    this.host = this.extract(this.appProps, 'services.host', '');
    this.paths = this.extract(this.appProps, 'services.paths', {});
    this.channels = this.extract(this.appProps, 'crossChannels', {});
    this.securityConf = this.extract(this.appProps, 'securityConf', {});
    this.eventRequestName = this.extract(
      this.appProps,
      'ctts.eventRequestName',
      'send-request-dp'
    );
    this.constants = this.extract(this.appProps, 'ctts', {});
    this.subscribeEventsPortalsCommunicator();
  }

   /**
   * Registro de eventos para la comunicacion entre el portal y las sub aplicaciones.
   */
  subscribeEventsPortalsCommunicator() {
    window.addEventListener(this.channels.readyChildren, ({ detail }) => 
      this.registerApp(detail)
    );
    window.addEventListener(this.channels.userSession, ({ detail }) =>
      this.getDataPortal(detail, 'user')
    );
    window.addEventListener(this.channels.tsec, ({ detail }) =>
      this.getDataPortal(detail, 'tsec')
    );
    window.addEventListener(this.channels.listApps, ({ detail }) =>
      this.getDataPortal(detail, 'applications')
    );
    window.addEventListener(this.channels.getComponents, ({ detail }) =>
      this.getComponentesByApplication(detail.applicationId)
    );
    window.addEventListener(this.channels.childDataParams, ({ detail }) =>
      this.getParamsDataChildren(detail)
    );
    window.addEventListener(this.channels.sendNotification, ({ detail }) => {
        this.getDataPortal(detail, 'notification-toast');
      }
    );
    window.addEventListener(this.channels.showDialogExpiredSession, ({ detail }) => {
      this.getDataPortal(detail, 'dialog-session-expired');
    }
  );
  }

  async getComponentesByApplication(applicationId) {
    let components;
    // === Bifurcacion de logica para obtener la informacion por memoria o por servicios ===
    const dataRemoteMenuApps = this.extract(this.constants, 'settingsConf.remoteDataComponents', true);
    let rols = this.currentRolesId;
    if (!dataRemoteMenuApps) {
      // === Logica de informacion desde memoria ===
      await this.sleepIterator(200);
      const _applicationsComponents = this.extract(this.appProps, 'applicationsComponentsItems', []);
      components= await this.glDmSession.findItemsComponentsLocalByProfile(
        rols,
        applicationId,
        _applicationsComponents
      );
      
    } else {

    // === Logica de informacion desde servicios ===
    const keyPath = 'getComponentByApplication';
    let _path = this.extract(this.paths, `security.${keyPath}`, '');
    _path = _path.replace('{applicationId}', applicationId);
    this.glDmSession.paths[keyPath] = _path;
    const body = {
      rolesId: this.currentRolesId,
    };
    try {
      components = await this.glDmSession.findAplicationsAndComponentesByProfile({
        keyPath,
        body,
      });
    } catch (e) {
      console.log('getComponentesByApplication[ERROR]', e)
      components = [];
    }
  }
    const detail = {};
    detail.iframe = this.findIframe(applicationId);
    detail.eventName = `${this.channels.getComponents}-child-${applicationId}`;
    detail.components = components;
    this.glCommunicator._sendMessageToChild(detail);
  }

  /**
  * metodo realiza la busqueda de el iframe de sub aplicacion por el id.
  * @param {String} id - identificador de iframe 
  */
  findIframe(id) {
    const _cmpPortal = this.portal;
    if (!_cmpPortal) {
      return null;
    }
    return _cmpPortal.element(`#iframe-app-${id}`);
  }

  /**
  * Se registra la aplicacion en memoria para una mejor gestion.
  * @param {Object} detail - detalle de la aplicacion
  */
  registerApp(detail) {
    if (this.appIdsregister.indexOf(detail.applicationId) < 0) {
      this.appIdsregister.push(detail.applicationId);
    }
  }

  /**
  * Generacion de valores por defecto de un tab de aplicaciones
  * @param {String} id - identificador de la aplicacion
  */
  generateDataTab(id) {
    const componentType = {
      name: 'iframe',
    };
    return { id, componentType };
  }

  /**
  * Flujo de datos entre el portal y las sub aplicaciones
  * @param {Object} detail - informacion de la aplicacion
  * @param {String} type - tipo de informacion a administrar
  */
  async getDataPortal(detail, type) {
    const { applicationId } = detail;
    switch (type) {
      case 'user':
        detail.userSession = this.glDmSession.getUser();
        break;
      case 'tsec':
        if (detail.refreshTsec) {
          await this.glAuthServ.runEmployeeAuthentication();
        }
        detail.tsec = this.glDmSession.getTsec(this.appProps.tsecNameSession);
        break;
      case 'applications':
        detail.applications = this.glDmSession.getApplications();
        break;
      case 'notification-toast':
        const { message, type } = detail;
        this.showToastNoti({ message, type})
        break;  
      case 'dialog-session-expired':
        this.glMdlErrorAuth.classList.remove('hide');
        this.glMdlErrorAuth.open();
        break;  
        
    }
    detail.iframe = this.findIframe(applicationId);
    if (detail.withResponse) {
      detail.eventName = `${detail.eventName}-child-${applicationId}`;
    }
    this.glCommunicator._sendMessageToChild(detail);
  }

  /**
  * Obtencion de parametros de una sub aplicacion
  * @param {Object} detail - informacion de la aplicacion
  */
  getParamsDataChildren(detail) {
    const { applicationId, idAppIframe } = detail;
    const _iframe = this.findIframe(applicationId);
    const _iframeDatParams = this.findIframe(idAppIframe);
    detail.iframe = _iframe;
    if (_iframeDatParams) {
      try {
        detail.paramsData = JSON.parse(_iframeDatParams.dataset.detail);
      } catch (e) {
        detail.paramsData = {};
      }
    }
    if (detail.withResponse) {
      detail.eventName = `${detail.eventName}-child-${applicationId}`;
    }
    this.glCommunicator._sendMessageToChild(detail);
  }

    /**
  * Envio de informacion de padre - hijo
  * @param {Object} _arguments - informacion de la aplicacion
  */
  propagateChildrenEvent(_arguments) {
    const _cmpPortal = this.portal;
    if (!_cmpPortal) {
      return null;
    }
    const _iframes = _cmpPortal.elementsAll('iframe');
    _iframes.forEach((item) => { 
      _arguments.iframe = item; 
      this.glCommunicator._sendMessageToChild(_arguments);
    });
  }

  /**
   * Metodo que obtiene el componente de container apps
   * @returns {Object} Componente cells-portals-container-apps-ppgc
   */
  get portal() {
    return this.element('#portal-container');
  }

  /**
   * Metodo que obtiene el componente de gestion de sesion
   * @returns {Object} cells-portals-core-session-dm-ppgc
   */
  get glDmSession() {
    return document.querySelector('#session-dm-gl-cmp');
  }

  /**
   * Metodo que obtiene el componente de autenticacion y gestion de servicios
   * @returns {Object} cells-portals-core-auth-service-ppgc
   */
  get glAuthServ() {
    return document.querySelector('#auth-serv-gl-cmp');
  }

  /**
   * Metodo que obtiene el componente spinner
   * @returns {Object} bbva-foundations-spinner
   */
  get glSpinner() {
    return document.querySelector('#spinner-gl-cmp');
  }

  /**
  * Metodo que obtiene el componente toast notification
  * @returns {Object} bbva-web-notification-toast
  */
  get glNotification() {
    return document.querySelector('#notification-gl-cmp');
  }

  /**
  * Metodo que obtiene el componente modal de error de autenticacion
  * @returns {Object} bbva-web-template-modal
  */
  get glMdlErrorAuth() {
    return document.querySelector('#modal-error-auth-serv-gl-cmp');
  }

  /**
  * Metodo que obtiene el componente de comunicacion entre el portal y las sub aplicaciones
  * @returns {Object} cells-portals-core-communication-dm-ppgc
  */
  get glCommunicator() {
    return document.querySelector('#con-portal-dm-gl-cmp');
  }

  /**
  * Metodo muestra un mensaje tipo toast en la aplicacion
  * @param {String} message - mensaje de de notificacion
  * @param {String} type - tipo de notificacion
  */
  showToastNoti({ message = '', type = 'info' }) {
    this.glNotification.setAttribute('class', `noti-${type}`);
    this.glNotification.innerHTML = `<p>${message}</p>`;
    this.glNotification.opened = true;
  }

  /**
  * Metodo que valida mediante una promesa la presencia de el TSEC
  */
  async awaitPresentTsec() {
    let clear;
    let count = 0;
    return new Promise((resolve, reject) => {
      clear = setInterval(() => {
        count = count + 1;
        const _tsec = window.sessionStorage.getItem(this.appProps.tsecNameSession);
        if (_tsec) {
          resolve(true);
          clearTimeout(clear);
        } else if (count > 80) {
          reject(false);
        }
      }, 500);
    });
  }

  /**
  * Metodo de iniciacion
  */
  async initMasterPage() {
    await this.updateComplete;
    this.overwriteOnPageMethods();
  }

  beforeOnPageEnter() {}

  beforeOnPageLeave() {}

  /**
  * Metodo interceptor antes de ejecucion
  */
  interceptorBeforeMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      fn.apply(object);
      originalMethod.apply(object, arguments);
    };
  }

  /**
  * Metodo interceptor despues de ejecucion
  */
  interceptorAfterMethod(object, method, fn) {
    let originalMethod = object[method];
    if (!fn) {
      fn = () => {};
    }
    object[method] = () => {
      originalMethod.apply(object, arguments);
      fn.call(object);
    };
  }

  /**
  * Metodo de sobrescritura de metodos
  */
  overwriteOnPageMethods() {
    if (this.onPageEnter && typeof this.onPageEnter === 'function') {
      this.interceptorBeforeMethod(this, 'onPageEnter', this.beforeOnPageEnter);
    }
    if (this.onPageLeave && typeof this.onPageLeave === 'function') {
      this.interceptorBeforeMethod(this, 'onPageLeave', this.beforeOnPageLeave);
    }
  }

  /**
  * Metodo que se utiliza en el render de las paginas
  */
  content(contentHtml, titlePage = '') {
    return html` <container-app-template page-title="${titlePage}">
      <div class="container-page" slot="app-main-content">${contentHtml}</div>
    </container-app-template>`;
  }
}
