import { BbvaFoundationsGridDefaultTemplateBase } from '@bbva-web-components/bbva-foundations-grid-default-template-base';

class ContainerAppTemplate extends BbvaFoundationsGridDefaultTemplateBase {}
customElements.define('container-app-template', ContainerAppTemplate);