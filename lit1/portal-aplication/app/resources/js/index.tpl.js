const VALUES = {
  AC: 'auth-serv-gl-cmp',
  MDLAUTH: 'modal-error-auth-serv-gl-cmp',
  SPIN: 'spinner-gl-cmp',
  NOT: 'notification-gl-cmp',
  SES: 'session-dm-gl-cmp',
  CON: 'con-portal-dm-gl-cmp'
};

const hash = async (string) => {
  const utf8 = new TextEncoder().encode(string);
  return crypto.subtle.digest('SHA-512', utf8).then((hashBuffer) => {
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray
      .map((bytes) => bytes.toString(16).padStart(2, '0'))
      .join('');
    return hashHex;
  });
}

const awaitAppConf = () => {
  let clear;
  let count = 0;
  let accessControl = auhServ();
  return new Promise((resolve, reject) => {
    clear = setInterval(() => {
      count = count + 1;
      if (appConf() && accessControl && accessControl.begin) {
        resolve(true);
        clearTimeout(clear);
      } else if (count > 80) {
        reject(false);
      }
    }, 500);
  });
};

const awaitUserIdSession = () => {
  let clear;
  let count = 0;
  return new Promise((resolve, _reject) => {
    clear = setInterval(() => {
      count = count + 1;
      if (window.sessionStorage.getItem('userId')) {
        resolve(true);
        clearTimeout(clear);
      } else if (count > 80) {
        resolve(false);
      }
    }, 500);
  });
};

const showLoadingGlobalApp = (show, modeSpinner) => {
  const spinnerGlobal = document.querySelector(`#${VALUES.SPIN}`);
  if (show) {
    if (modeSpinner) {
      spinnerGlobal.classList.remove('hide');
      return;
    }
    auhServ().showMaskLoading();
  } else {
    if (modeSpinner) {
      spinnerGlobal.classList.add('hide');
      return;
    }
    auhServ().hideMaskLoading();
  }
};

const elDOM = (selector) => {
  return document.querySelector(selector);
};

const appConf = () => {
  return window.AppConfig;
};

const auhServ = () => {
  return elDOM(`#${VALUES.AC}`);
};

const applySessionConfg = async () => {
  auhServ().disableValidateSession = false;
  auhServ().urlGrantingTicket = `${appConf().services.host}/${
    appConf().securityConf.pathGT
  }`;
  auhServ().appId = appConf().securityConf.aapId;
  const enableVBank = appConf().enableVBank;
  const tsecNameSession = appConf().tsecNameSession || 'tsec-portal';
  let tsec = window.sessionStorage.getItem(tsecNameSession);
  if (!tsec && !enableVBank) {
    await auhServ().begin();
  } else {
    auhServ().hideMaskLoading();
  }
  if (enableVBank) {
    tsec = await hash(tsecNameSession);
    window.sessionStorage.setItem(tsecNameSession, tsec);
    window.sessionStorage.setItem('userId', 'PXXXXXXX1');
  }
  await awaitUserIdSession();
  let userIdsession = window.sessionStorage.getItem('userId');
  let userSession = {};
  const isLba = appConf().dataUserType === 'lba';
  const isCrypto = appConf().securityConf.enableUserCrypto;
  const userDm = document.querySelector('cells-portals-core-session-dm-ppgc');
  if (isCrypto) {
    const stream = userIdsession;
    const cryptoKey = appConf().securityConf.cryptoKey;
    const codecFormat = appConf().securityConf.codecFormat;
    const _responseCrypto = await userDm.createCryptoDocument({ stream, cryptoKey, codecFormat });
    console.log('_responseCrypto', _responseCrypto);
    userIdsession = _responseCrypto.detail.data.document;
  }
  //createCryptoDocument
  let pathGetUser;
  if (isLba) {
    pathGetUser = `${appConf().services.paths.security.listBusinessAgents}=${userIdsession}`;
  } else {
    pathGetUser = `${appConf().services.paths.security.picIdm}=${userIdsession}`;
  }
  if (userIdsession || enableVBank) {
    const settingsUser = {
      path: pathGetUser,
      host: appConf().services.host,
    };
    try {
      userSession = await auhServ().requestAwait(settingsUser);
      userSession = userSession?.detail?.data? userSession?.detail?.data[0]: {};
      
      userSession = await userDm.formatUserSession(userSession, isLba ? 'list-agents' : 'idm-legacy');
    }catch(e) {
      console.log(e);
      if (!e.messages){
        userSession.messageError = e; 
      } else {
        userSession.messageError = e?.messages[0]?.message? e.messages[0].message : null; 
      }
    }
  }
  window.dispatchEvent(
    new CustomEvent('on-user-session-load', {
      detail: userSession,
      bubbles: true,
      composed: true,
    })
  );
};

const addListenersApp = () => {
  auhServ().addEventListener('employee-authentication-success', (_result) => {
    showLoadingGlobalApp(false);
    window.dispatchEvent(
      new CustomEvent('done-success-auth', {
        detail: true,
        bubbles: true,
        composed: true,
      })
    );
  });

  auhServ().addEventListener('idp-assertion-error', () => {
    console.log('idp-assertion-error');
    document.querySelector(`#${VALUES.MDLAUTH}`).classList.remove('hide');
    document.querySelector(`#${VALUES.MDLAUTH}`).open();
    showLoadingGlobalApp(false);
  });

  auhServ().addEventListener('granting-ticket-error', ({detail}) => {
	showLoadingGlobalApp(false);
	elDOM(`#${VALUES.NOT}`).setAttribute('class', 'noti-danger');
    elDOM(`#${VALUES.NOT}`).innerHTML = `<p class="error-host-service-certificate">Error de acceso al servicio de Autenticación, por favor verificar el siguiente enlace <a href="${appConf().services.host}" target="_blank" >verificar</a></p>`;
    elDOM(`#${VALUES.NOT}`).opened = true;
    console.log('granting-ticket-error', detail);
  });

  auhServ().addEventListener('idp-error', ({detail}) => {
    console.log('idp-error', detail);
    document.querySelector(`#${VALUES.MDLAUTH}`).classList.remove('hide');
    document.querySelector(`#${VALUES.MDLAUTH}`).open();
    showLoadingGlobalApp(false);
  });

  auhServ().addEventListener('sso-error', () => {
    console.log('sso-error');
    showLoadingGlobalApp(false);
  });

  auhServ().addEventListener('error-request-dp', ({detail}) => {
    console.log('error-request-dp', detail);
    showLoadingGlobalApp(false);
  });

  window.addEventListener('on-global-loading', ({ detail }) => {
    showLoadingGlobalApp(detail);
  });

  elDOM(`#${VALUES.MDLAUTH}`).addEventListener('button-click', () => {
      window.location = appConf().securityConf.externalDMZ;
  });
};

const initializeConfigurations = async () => {
  await awaitAppConf();
  elDOM(`#${VALUES.MDLAUTH}`).classList.remove('hide');
  elDOM(`#${VALUES.MDLAUTH}`).linkHref = appConf().securityConf.externalDMZ;
  addListenersApp();
  await applySessionConfg();
};

document.addEventListener('DOMContentLoaded', async () => {
  await initializeConfigurations();
});
