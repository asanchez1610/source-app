const express = require('express');
const fs = require('fs');
const path = require('path');
const folderPath = path.join(__dirname, 'stubs');
const app = express();
app.use((_req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE");
    next();
});

const _buildResponse = (req, res, _json, _headers) => {
  if (_headers) {
    for (const [key, value] of Object.entries(_headers)) {
      res.setHeader(key, value);
    }
  }
  res.status(_json.statusResponse ? _json.statusResponse : 200);
  if (_json.response) {
    res.json(_json.response);
  } else {
    res.end();
  }
};

fs.readdir(folderPath, (err, files) => {
  files.forEach((file) => {
    try {
      console.log(file);
      const data = fs.readFileSync(
        path.join(__dirname, `stubs/${file}`),
        "utf8"
      );
      const _json = JSON.parse(`${data}`);
      const method = _json.method.toLowerCase();
      const _headers = _json.headers;
      switch (method) {
        case "get":
          app.get(_json.path, (req, res) =>
            _buildResponse(req, res, _json, _headers)
          );
          break;
        case "post":
          app.post(_json.path, (req, res) =>
            _buildResponse(req, res, _json, _headers)
          );
          break;
        case "delete":
          app.delete(_json.path, (req, res) =>
            _buildResponse(req, res, _json, _headers)
          );
          break;
        case "put":
          app.put(_json.path, (req, res) =>
            _buildResponse(req, res, _json, _headers)
          );
          break;
        case "patch":
          app.patch(_json.path, (req, res) =>
            _buildResponse(req, res, _json, _headers)
          );
          break;
        default:
          break;
      }
    } catch (_err) {
      console.log(_err);
    }
  });
});

app.get("/", (req, res) => {
  res.send(`<div style="text-shadow: 2px 2px 3px rgba(66, 68, 90, 1);
                            font-size: 2.5em;
                            width: 100vw; 
                            height: 100%; 
                            display: grid; 
                            place-content: center;">
                    <h1>D3lin4Rest Emulator</h1>
              </div>`);
});

app.listen(3000);
